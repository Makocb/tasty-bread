#include <iostream>
#include <iomanip>
#include <vector>

using std::cout;
using std::endl;
using std::setw;
using std::vector;

//Constants
//Column-Width used by stdout
const int kColWidth = 5;

//Number of elements to run the sim on, max value 9
const int kNumElements = 5;

//if you want to change for whatever reason the number of piles, default is 3.
const int kNumPiles = 3;
//end

//is this ok?, its pretty global
int numSteps = 0;

//Print given vector to stdout
void PrintVector(vector<int>& vec)
{
    for (int i = 0; i < vec.size(); i++) {
        cout << vec[i] << ' ';
    }
}

//initialize how the elements are distributed on the piles
//example distribution with 5 elements:
//------------------
//    1    .    .
//    2    .    .
//    3    .    .
//    4    .    .
//    5    .    .
//------------------
vector<vector<int>> InitPiles() {
    vector<int> pile1, pile2, pile3;

    for (int i = 0; i < kNumElements; i++) {
        pile1.push_back(kNumElements - i);
    }
    PrintVector(pile1);
    cout << endl;
        
    return {
        pile1,
        pile2,
        pile3
    };
}

//When a cell has no value, print a placeholder ".".
char PrintDotIfEmpty(vector<int>& pile, int index) {
    return pile.size() > index ? pile[index] + 48 : '.'; 
}

//print the current pile situation to stdout
void PrintPiles(vector<vector <int>>& piles) {
    for (int i = kNumElements - 1; i >= 0; i--) {
        char cell1, cell2, cell3;

        cout << setw(kColWidth) << PrintDotIfEmpty(piles[0], i)
             << setw(kColWidth) << PrintDotIfEmpty(piles[1], i)
             << setw(kColWidth) << PrintDotIfEmpty(piles[2], i)
        << endl;
    }
    cout << endl;
}

//Checks if a move is valid; The element you put on top needs to be smaller always.
bool ValidateMove(int movingElement, int underElement)
{
    return movingElement < underElement;
}

//Moves an element from one pile to another
void MoveElement(vector<vector <int>>& piles, int sourcePileIndex, int targetPileIndex) {
    cout << "Move " << numSteps++ << " -> ";

    vector<int>& sourcePile = piles[sourcePileIndex];
    vector<int>& targetPile = piles[targetPileIndex];

    if (sourcePile.empty()) {
        cout << " no elements available on pile " << sourcePileIndex + 1
             << ", aborting... :("
        << endl;
        abort();
    }

    cout << "moving top element from pile " << sourcePileIndex + 1
         << " (" << sourcePile.back() << ")"
         << " to pile " << targetPileIndex + 1
    << endl << endl;

    if (!targetPile.empty() && !ValidateMove(sourcePile.back(), targetPile.back())) {
        cout << "Illegal move, aborting... :(" << endl;
        abort();
    }
    try {
        targetPile.push_back(
            sourcePile.back()
        );
        sourcePile.pop_back();
    } catch (const std::exception& e) {
        cout << "unexpected outcome, reason below" << endl;
        cout << e.what() << endl;
        cout << "aborting... :(" << endl;
        abort();
    }
    
    PrintPiles(piles);
}

// Dummy Function works only with 5 elements, produces a predefined result.
void SimulatedSequence(vector<vector <int>>& piles) {

    //Print the pile situation before any element has moved (move 0)
    PrintPiles(piles);

    //execute the steps to solve the problem

    //Move 0
    //  1    .    .
    //  2    .    .
    //  3    .    .
    //  4    .    .
    //  5    .    .

    //Move 1
    MoveElement(piles, 0, 2);
    //  .    .    .
    //  2    .    .
    //  3    .    .
    //  4    .    .
    //  5    .    1
    

    //Move 2
    MoveElement(piles, 0, 1);
    //  .    .    .
    //  .    .    .
    //  3    .    .
    //  4    .    .
    //  5    2    1
    

    //Move 3
    MoveElement(piles, 2, 1);
    //  .    .    .
    //  .    .    .
    //  3    .    .
    //  4    1    .
    //  5    2    .
    

    //Move 4
    MoveElement(piles, 0, 2);
    //  .    .    .
    //  .    .    .
    //  .    .    .
    //  4    1    .
    //  5    2    3
    

    //Move 5
    MoveElement(piles, 1, 0);
    //  .    .    .
    //  .    .    .
    //  1    .    .
    //  4    .    .
    //  5    2    3
    

    //Move 6
    MoveElement(piles, 1, 2);
    //  .    .    .
    //  .    .    .
    //  1    .    .
    //  4    .    2
    //  5    .    3
    

    //Move 7
    MoveElement(piles, 0, 2);
    //  .    .    .
    //  .    .    .
    //  .    .    1
    //  4    .    2
    //  5    .    3
    

    //Move 8
    MoveElement(piles, 0, 1);
    //  .    .    .
    //  .    .    .
    //  .    .    1
    //  .    .    2
    //  5    4    3
    

    //Move 9
    MoveElement(piles, 2, 1);
    //  .    .    .
    //  .    .    .
    //  .    .    .
    //  .    1    2
    //  5    4    3
    

    //Move 10
    MoveElement(piles, 2, 0);
    //  .    .    .
    //  .    .    .
    //  .    .    .
    //  2    1    .
    //  5    4    3
    

    //Move 11
    MoveElement(piles, 1, 0);
    //  .    .    .
    //  .    .    .
    //  1    .    .
    //  2    .    .
    //  5    4    3

    //Move 12
    MoveElement(piles, 2, 1);
    //  .    .    .
    //  .    .    .
    //  1    .    .
    //  2    3    .
    //  5    4    .

    //Move 13
    MoveElement(piles, 0, 2);
    //  .    .    .
    //  .    .    .
    //  .    .    .
    //  2    3    .
    //  5    4    1

    //Move 14
    MoveElement(piles, 0, 1);
    //  .    .    .
    //  .    .    .
    //  .    2    .
    //  .    3    .
    //  5    4    1

    //Move 15
    MoveElement(piles, 2, 1);
    //  .    .    .
    //  .    1    .
    //  .    2    .
    //  .    3    .
    //  5    4    .

    //Move 16
    MoveElement(piles, 0, 2);
    //  .    .    .
    //  .    1    .
    //  .    2    .
    //  .    3    .
    //  .    4    5

    //Move 17
    MoveElement(piles, 1, 2);
    //  .    .    .
    //  .    .    .
    //  .    2    .
    //  .    3    1
    //  .    4    5

    //Move 18
    MoveElement(piles, 1, 0);
    //  .    .    .
    //  .    .    .
    //  .    .    .
    //  .    3    1
    //  2    4    5

    //Move 19
    MoveElement(piles, 2, 0);
    //  .    .    .
    //  .    .    .
    //  .    .    .
    //  1    3    .
    //  2    4    5

    //Move 20
    MoveElement(piles, 0, 1);
    //  .    .    .
    //  .    .    .
    //  .    1    .
    //  .    3    .
    //  2    4    5

    //Move 21
    MoveElement(piles, 0, 2);
    //  .    .    .
    //  .    .    .
    //  .    1    .
    //  .    3    2
    //  .    4    5

    //Move 22
    MoveElement(piles, 1, 2);
    //  .    .    .
    //  .    .    .
    //  .    .    1
    //  .    3    2
    //  .    4    5

    //Move 23
    MoveElement(piles, 1, 0);
    //  .    .    .
    //  .    .    .
    //  .    .    1
    //  .    .    2
    //  3    4    5

    //Move 24
    MoveElement(piles, 2, 1);
    //  .    .    .
    //  .    .    .
    //  .    .    .
    //  .    1    2
    //  3    4    5

    //Move 25
    MoveElement(piles, 2, 0);
    //  .    .    .
    //  .    .    .
    //  .    .    .
    //  2    1    .
    //  3    4    5

    //Move 26
    MoveElement(piles, 1, 0);
    //  .    .    .
    //  .    .    .
    //  1    .    .
    //  2    .    .
    //  3    4    5

    //Move 27
    MoveElement(piles, 1, 2);
    //  .    .    .
    //  .    .    .
    //  1    .    .
    //  2    .    4
    //  3    .    5

    //Move 28
    MoveElement(piles, 0, 2);
    //  .    .    .
    //  .    .    .
    //  .    .    1
    //  2    .    4
    //  3    .    5

    //Move 29
    MoveElement(piles, 0, 1);
    //  .    .    .
    //  .    .    .
    //  .    .    1
    //  .    .    4
    //  3    2    5

    //Move 30
    MoveElement(piles, 2, 1);
    //  .    .    .
    //  .    .    .
    //  .    .    .
    //  .    1    4
    //  3    2    5

    //Move 31
    MoveElement(piles, 0, 2);
    //  .    .    .
    //  .    .    .
    //  .    .    3
    //  .    1    4
    //  .    2    5

    //Move 32
    MoveElement(piles, 1, 0);
    //  .    .    .
    //  .    .    .
    //  .    .    3
    //  .    .    4
    //  1    2    5

    //Move 33
    MoveElement(piles, 1, 2);
    //  .    .    .
    //  .    .    2
    //  .    .    3
    //  .    .    4
    //  1    .    5

    //Move 34
    MoveElement(piles, 0, 2);
    //  .    .    1
    //  .    .    2
    //  .    .    3
    //  .    .    4
    //  .    .    5
    
    //end
}

void Recursion(int start, int free, int reserved, int N)
{
    if (N > 1)
    {
        Recursion(start, reserved, free, N-1);
                 cout << start <<' '<< free << endl;
        Recursion(reserved, free, start, N-1);
    }
}

int main()
{
    vector<vector<int>> piles = InitPiles();
    SimulatedSequence(piles);
    // Recursion(1,2,3,10);
    return 0;
}