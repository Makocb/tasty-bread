#include <iostream>
#include "bread.inl"
#include "tinyxml2.h"

using namespace tinyxml2;

class Baguette : public Bread
{

	public:
		void Init() {
			fullness = 1;
			volume = 200;
			weight = 500;
		}

		double GetFullness() { return fullness; }

		int GetVolume() { return volume; }

		int GetWeight() { return weight; }

		void EatHalf() { fullness *= 0.5; }

};

int main()
{
	//this soesnt work, just was testing stuff on the internet
	/*
	//this doesnt cause an Error

	tinyxml2::xmldocument xmldoc;
	tinyxml2::xmlerror err = xmldoc.loadfile("https://www.der-beck.de/");
	xmlnode* proot = xmldoc.firstchild();


	//this causes an Error

	xmlelement* pelement = proot->firstchildelement("tittle");
	if (pelement == nullptr) return xml_error_parsing_element;*/

	Baguette baguette;

	baguette.Init();
	baguette.EatHalf();
	std::cout << baguette.GetFullness() << std::endl;
}