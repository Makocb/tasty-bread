// Base class
class Bread {

    protected:
        double fullness;
        int volume;
        int weight;

    public:
        // pure virtual functions providing interface framework.
        virtual double GetFullness() = 0;
        virtual int GetVolume() = 0;
        virtual int GetWeight() = 0;
        // end

        // The 'this' pointer is used to retrieve the object's fullness 
        // hidden by the local variable 'fullness' 
        void SetFullness(int fullness) {
            this->fullness = fullness;
        }
        void SetVolume(int volume) {
            this->volume = volume;
        }
        void SetWeight(int weight) {
            this->weight = weight;
        }

};
